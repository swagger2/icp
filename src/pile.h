/**
 * @file   pile.h
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  header file for Pile class
 */

#ifndef PILE_H
#define PILE_H

#include <vector>

#include "card.h"


/**
 * @brief Represents a pile. Contains only the information about a pile,
 *        including the cards in it.
 */
class Pile {

public: // enums & classes

  /**
   * @brief Pile kinds.
   */
  enum class Kind {
    Stock,
    Waste,
    Tableau,
    Foundation,
    COUNT
  };

public: // methods

  Pile();
  Pile(Kind kind);
  Pile(Kind kind, int num);

  Pile & operator=(Pile const& other);


  std::string to_string() const;
  std::string name() const;


  /**
   * @return Kind of the pile.
   */
  Kind kind() const { return m_kind; }

  /**
   * @return Vector of pointers to cards in the pile.
   */
  std::vector<Card*> const& cards() const { return m_cards; }

  Card *top() const;

  void pushCard(Card *card);
  Card *popCard();

  std::vector<Card*> popAllCards();
  std::vector<Card*> popAllCardsSince(Card *card);
  void pushAllCards(std::vector<Card*> cards);

  void removeAllCards();

public: // static methods

  /**
   * @return Mapping of kind names to kinds.
   */
  static std::map<Kind, std::string> const& kindNames() {
    return m_kindNames;
  }

  static std::string getKindName(Kind kind);

private: // static members

  static const std::map<Kind, std::string> m_kindNames;

private: // instance members

  Kind m_kind = Kind::COUNT;

  int m_num = 0;

  std::vector<Card*> m_cards;
};


#endif

