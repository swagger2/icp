/**
 * @file   util.h
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  header file for utility functions & classes
 */

#ifndef UTIL_H
#define UTIL_H

#include <vector>
#include <string>
#include <sstream>
#include <memory>
#include <cstdio>

#include <tinydir.h>


int mkdir(std::string const& path);


/**
 * @brief Platform-dependent path separator.
 */
constexpr char PATHSEP =
#ifdef _WIN32
  '\\'
#else
  '/'
#endif
  ;


std::string pathjoin(std::string const& path1, std::string const& path2);


/**
 * @brief Wrapper for `tinydir_dir` struct for automatic closing
 *        in the destructor.
 */
class TinyDir {

  tinydir_dir dir;

public:

  ~TinyDir() { tinydir_close(&dir); }

  std::vector<std::string> getFilenames(std::string const& path);
};


/**
 * @brief Ring buffer providing `put()` and `pop()` operations.
 */
template <typename T>
class RingBuffer {

  std::vector<T> m_buffer;

  bool m_empty = true;

  size_t
      m_first = 0,
      m_last  = 0;

public:

  /**
   * @brief Initialize buffer -- set maximum capacity.
   * @param cap Maximum capacity.
   */
  RingBuffer(size_t cap) :
    m_buffer(cap)
  {}

  /**
   * @brief Empty the buffer.
   */
  void clear() {
    m_first = m_last = 0;
    m_empty = true;
  }

  /**
   * @brief Check whether the buffer is full.
   * @return `true` if the buffer is full; `false` otherwise.
   */
  bool full()  const { return !m_empty && m_first == m_last; }

  /**
   * @brief Check whether the buffer is empty.
   * @return `true` if the buffer is empty; `false` otherwise.
   */
  bool empty() const { return m_empty; }

  /**
   * @brief Return current ring buffer size.
   * @return Number of items in the ring buffer.
   */
  size_t size() const {
    auto bufsz = m_buffer.size();
    if (full()) { return bufsz; }
    return (m_last - m_first + bufsz) % bufsz;
  }

  /**
   * @brief Put an item in the buffer, replacing
   *        the least recent item if the buffer is full.
   * @param item Item to put into the buffer.
   */
  void put(T const& item) {
    bool wasFull = full();

    m_buffer[m_last] = item;
    m_last = (m_last+1) % m_buffer.size();
    m_empty = false;

    if (wasFull) { m_first = m_last; }
  }

  /**
   * @brief Remove and return the last item in the buffer.
   *        UB if the buffer is empty.
   * @return The last inserted item.
   */
  T pop() {
    m_last = (m_last ? m_last : m_buffer.size()) - 1;
    m_empty = full();  // The full-empty paradox

    return m_buffer[m_last];
  }
};



/**
 * @brief Vector of limited size; holds unique pointers to
 *        automatically created (and deleted) objects.
 * @param T Type of items in the vector. Must have a default constructor.
 */
template <typename T>
class UniquePtrVec {

public:

  /**
   * @brief Initialize (set maximum size).
   * @param cap Maximum size.
   */
  UniquePtrVec(size_t cap) :
    m_cap(cap)
  {}

  /**
   * @return Maximum vetcor size.
   */
  size_t cap()  const { return m_cap; }

  /**
   * @return Current vector size.
   */
  size_t size() const { return m_items.size(); }

  /**
   * @return Remove all items (and delete them).
   */
  void clear() { m_items.clear(); }

  /**
   * @brief Push a new unique pointer to the default-constructed
   *        item if the current size is not the maximum size.
   * @return `true` if a new item was pushed, `false` otherwise.
   */
  bool makeNew() {
    if (m_items.size() == m_cap) { return false; }
    m_items.push_back( std::unique_ptr<T>(new T) );
    return true;
  }

  /**
   * @brief Remove and delete item at index `idx`.
   * @return New index (`idx`-1 if the item was last, otherwise `idx`).
   */
  int remove(int idx) {
    m_items.erase(m_items.begin() + idx);
    if (static_cast<size_t>(idx) == m_items.size()) { idx--; }
    return idx;
  }

  /**
   * @return Item at index `idx`.
   */
  T & operator[] (size_t idx) {
    return *m_items.at(idx).get();
  }

private:

  size_t m_cap;

  std::vector<std::unique_ptr<T>> m_items;
};


/* Convert letter case */
std::string & to_upper(std::string & str);
std::string & to_lower(std::string & str);
std::string      upper(std::string   str);
std::string      lower(std::string   str);

/* Read input */
std::string readline(std::string const& prompt = "");
std::stringstream readlineStream(std::string const& prompt = "");

/* Ask for confirmation */
bool confirm(std::string prompt, bool def = false);
bool confirm(const char *prompt, bool def = false);
bool confirm(bool def = false);


#endif

