/**
 * @file   game.h
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  header file for Game class
 */

#ifndef GAME_H
#define GAME_H

#include <map>
#include <algorithm>
#include <random>

#include "card.h"
#include "pile.h"
#include "util.h"


/**
 * @brief This struct is an internal part of the `Game` class.
 *        It handles [de]serialization of game state.
 * @param NCARDS Number of cards in the game.
 * @param NPILES Number of piles in the game.
 */
template <size_t NCARDS, size_t NPILES>
struct GameState {

  friend class Game;

  /**
   * @brief Default constructor.
   */
  GameState() {}

  /**
   * @brief Serialize the game state into Cereal archive.
   */
  template<class Archive>
  void save(Archive & ar) const {

    for (Card const& card : cards) {
      ar( card.isUpturned() );
    }

    for (Pile const& pile : piles) {
      ar( pile.cards().size() );

      for (Card *card : pile.cards()) {
        ar( card - cards.data() );
      }
    }
  }

  /**
   * @brief Deserialize the game state from Cereal archive.
   */
  template<class Archive>
  void load(Archive & ar) {

    for (Card & card : cards) {
      bool isUpturned;
      ar( isUpturned );
      card.setUpturned(isUpturned);
    }

    for (Pile & pile : piles) {
      size_t size;
      ar( size );

      for (size_t i = 0; i < size; i++) {
        std::ptrdiff_t offset;
        ar( offset );
        pile.pushCard(&cards[offset]);
      }
    }
  }

private:

  std::array<Card, NCARDS> cards;
  std::array<Pile, NPILES> piles;
};


/**
 * @brief This class handles game logic (creating and moving cards / piles).
 *        It keeps history (for undo) and via the GameState struct handles
 *        saving / loading of games.
 */
class Game {

public: // constants, enums, typedefs

  static constexpr int

      /** @brief Number of cards in the game. */
      NUM_CARDS = static_cast<int>(Card::Rank::COUNT) *
                  static_cast<int>(Card::Suit::COUNT),

      /** @brief Number of all piles in the game. */
      NUM_F_PILES = static_cast<int>(Card::Suit::COUNT),

      /** @brief Number of tableau piles in the game. */
      NUM_T_PILES = 7,

      /** @brief Number of foundation piles in the game. */
      NUM_PILES = 2 + NUM_T_PILES + NUM_F_PILES;


  /**
   * @brief `std::array` of pointers to all cards in the game
   *        (compile-time fixed size required).
   */
  typedef std::array<Card*, NUM_CARDS> CardPtrArr;

  /**
   * @brief `std::array` of pointers to tableau piles
   *        (compile-time fixed size required).
   */
  typedef std::array<Pile*, NUM_T_PILES> TableauPilePtrArr;

  /**
   * @brief `std::array` of pointers to foundation piles
   *        (compile-time fixed size required).
   */
  typedef std::array<Pile*, NUM_F_PILES> FoundationPilePtrArr;


private: // constants, enums, typedefs

  typedef GameState<NUM_CARDS, NUM_PILES> State;


public: // methods

  Game();

  /**
   * @return Name of save if the game was saved / loaded.
   */
  std::string saveName() const { return m_saveName; }


  /* Restart game (initial state, cards are randomly shuffled anew) */
  void restart();

  /**
   * @return Pointer to the stock pile.
   */
  Pile *stock() const { return m_stock; }

  /**
   * @return Pointer to the waste pile.
   */
  Pile *waste() const { return m_waste; }

  /**
   * @return Array of pointers to the tableau piles.
   */
  TableauPilePtrArr const& tableauPiles() const { return m_tableauPiles; }

  /**
   * @return Array of pointers to the foundation piles.
   */
  FoundationPilePtrArr const& foundationPiles() const { return m_foundationPiles; }

  /* Get cards and piles by their name */
  Card *getCardByName(std::string name);
  Pile *getPileByName(std::string name);

  /* Move cards between stock and waste */
  bool nextFromStock();
  bool wasteToStock();

  /* Move a card to a pile */
  bool moveCard(Card *card, Pile *to);

  /* Provide a hint (which card can be moved) */
  bool hint(Card * & card, Pile * & pile);

  /* Undo last move */
  bool undo();

  /* Indication whether the game is finished */
  bool isFinished() const;

  /* Save / load */
  std::vector<std::string> getSavedGames() const;
  bool saveExists(std::string saveName) const;

  void saveGame(std::string saveName);
  void saveGamePath(std::string savePath);

  void loadGame(std::string saveName);
  void loadGamePath(std::string savePath);


private: // methods

  /* Constructor delegatees */
  void InitGroups();
  void InitCards();
  void InitPiles();
  void InitNameMaps();
  void SeedRand();

  /* Reset game to default state (empty piles etc.). */
  void reset();

  void shuffleAndDealCards();

  /* Helpers for `moveCard()` */
  bool canMoveCard(Card *card, Pile *to) const;
  bool canMoveToTableau(Card *card, Pile *to) const;
  bool canMoveToFoundation(Card *card, Pile *to) const;
  void doMoveCard(Card *card, Pile *to);


private: // static members

  static std::map<std::string, size_t> m_cardIdxByName;
  static std::map<std::string, size_t> m_pileIdxByName;

  static bool m_mapsInitialized;

  static std::default_random_engine m_RANDOM_ENGINE;


private: // instance members

  /* Folder with saved games */
  std::string m_savePath = "savegames";
  /* Name of the save of this game instance */
  std::string m_saveName = "";

  /* Current state & history */
  State m_state;
  RingBuffer<State> m_history;

  /* Cards & piles */
  CardPtrArr m_cards;

  Pile *m_stock;
  Pile *m_waste;
  TableauPilePtrArr    m_tableauPiles;
  FoundationPilePtrArr m_foundationPiles;
};


#endif

