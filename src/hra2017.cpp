/**
 * @file   hra2017.cpp
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  GUI version of the ICP-2017 project: Klondike card game.
 *
 * !! NOT IMPLEMENTED !!
 */

#include <iostream>

/**
 * @brief Start GUI interface of the game. !! NOT IMPLEMENTED !!
 */
int main() {
  std::cout << "NOT IMPLEMENTED" << std::endl;
}
