/**
 * @file   cliview.h
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  header file for CLI view
 */

#ifndef CLIVIEW_H
#define CLIVIEW_H


#include <string>
#include <sstream>
#include <vector>
#include <iomanip>
#include <iostream>
#include <cctype>

#include "game.h"
#include "util.h"


/**
 * @brief This class implements the CLI game interface.
 */
class CliView {

private: // classes, enums, typedefs

  typedef bool(CliView::*CmdFunc)(std::string const&, std::stringstream &);
  typedef std::map<std::string, CmdFunc> CmdMap;


public: // methods

  CliView();

  void start();


private: // methods

  Game & game() { return m_games[m_currGameIdx]; }

  /* The REPL loop */
  void loop();

  /* Display cards, piles, controls... */
  void show();

  /* Helpers for `show()` */
  void printHelp();
  void printStockWasteFoundation();
  void printTableau();

  /* User entered an invalid command */
  void invalidCommand(std::string const& cmdStr, std::stringstream & cmdStream);

  /* End the current instance of game */
  bool closeCurrentGame();
  /* Exit the application (close all games) */
  void exitApp();

  /*   USER COMMANDS   */

  /* Print usage */
  bool help(std::string const& cmdStr, std::stringstream & cmdStream);

  /* Start / restart / end one instance of game */
  bool newGame(std::string const& cmdStr, std::stringstream & cmdStream);
  bool restartGame(std::string const& cmdStr, std::stringstream & cmdStream);
  bool closeGame(std::string const& cmdStr, std::stringstream & cmdStream);

  /* Switch to previous / next instance of game */
  bool prevGame(std::string const& cmdStr, std::stringstream & cmdStream);
  bool nextGame(std::string const& cmdStr, std::stringstream & cmdStream);

  /* Save / load the current game */
  bool saveGame(std::string const& cmdStr, std::stringstream & cmdStream);
  bool loadGame(std::string const& cmdStr, std::stringstream & cmdStream);

  /* Deal a card from stock to waste */
  bool dealFromStock(std::string const& cmdStr, std::stringstream & cmdStream);
  /* Move a card to another pile */
  bool moveCard(std::string const& cmdStr, std::stringstream & cmdStream);

  /* Show a possible move */
  bool hint(std::string const& cmdStr, std::stringstream & cmdStream);

  /* Undo the last move in the current game */
  bool undo(std::string const& cmdStr, std::stringstream & cmdStream);

  /* Exit the application (close all games) */
  bool quit(std::string const& cmdStr, std::stringstream & cmdStream);


private: // static members

  static const CmdMap commandByName;


private: // instance members

  bool m_exit = false;

  UniquePtrVec<Game> m_games;

  int m_currGameIdx;
};


/**
 * @brief Print object, justified.
 * @param obj Object to print.
 * @param w Width [default 4].
 * @param sep Separator [default ' '].
 */
template<typename T>
void printJust(T obj, const int w = 4, const char sep = ' ') {
  std::cout << std::right << std::setw(w) << std::setfill(sep) << obj << sep;
}

/**
 * @brief Print object name in square brackets, justified.
 * @param obj Pointer to object to print (using its `name()` method).
 */
template <typename T>
void printName(T obj) {
  std::stringstream ss;
  ss << '[' << std::right << std::setw(2) << obj->name() << ']';
  printJust(ss.str());
}

/**
 * @brief Print object's string representation, justified.
 * @param obj Pointer to object to print (using its `to_string()` method).
 */
template <typename T>
void printStr(T obj) {
  printJust(obj->to_string() + " ");
}


#endif

