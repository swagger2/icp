/**
 * @file   game.cpp
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  implementation file for Game class
 */

#include "game.h"

#include <algorithm>
#include <fstream>
#include <random>
#include <stdexcept>

#include <cereal/archives/json.hpp>

#include "card.h"
#include "pile.h"
#include "util.h"


std::map<std::string, size_t> Game::m_cardIdxByName;
std::map<std::string, size_t> Game::m_pileIdxByName;
bool Game::m_mapsInitialized = false;

std::default_random_engine Game::m_RANDOM_ENGINE =
    std::default_random_engine(std::random_device{}());


/**
 * @brief Create cards and piles; shuffle cards and assign them to piles.
 */
Game::Game() :
  m_history(5)
{
  // Create groups of cards and piles (foundation, tableau, ...).
  InitGroups();

  // Create cards and piles.
  InitCards();
  InitPiles();

  // Initialize static mappings by names.
  if (!m_mapsInitialized) {
    InitNameMaps();
    m_mapsInitialized = true;
  }

  // Shuffle cards and assign them to piles.
  shuffleAndDealCards();
}

/**
 * @brief Create card and pile groups (foundation, tableau, ...).
 */
void Game::InitGroups() {

  // Initialize array of card pointers.
  std::transform(
    m_state.cards.begin(),
    m_state.cards.end(),
    m_cards.begin(),
    [](Card & card){ return &card; }
  );

  // Initialize pointers to stock & waste piles.
  m_stock = &m_state.piles[0];
  m_waste = &m_state.piles[1];

  // Initialize array of tableau pile pointers.
  std::transform(
    m_state.piles.begin() + 2,
    m_state.piles.begin() + 2 + NUM_T_PILES,
    m_tableauPiles.begin(),
    [](Pile & pile){ return &pile; }
  );

  // Initialize array of foundation pile pointers.
  std::transform(
    m_state.piles.begin() + 2 + NUM_T_PILES,
    m_state.piles.begin() + 2 + NUM_T_PILES + NUM_F_PILES,
    m_foundationPiles.begin(),
    [](Pile & pile){ return &pile; }
  );
}

/**
 * @brief Create cards.
 */
void Game::InitCards() {

  int i = 0;

  // All ranks.
  for (auto const& rank : Card::rankNames()) {
    auto const& rankValue = rank.first;

    // All suits for the rank.
    for (auto const& suit : Card::suitNames()) {
      auto const& suitValue = suit.first;

      *m_cards[i++] = Card(rankValue, suitValue);
    }
  }
}

/**
 * @brief Create piles.
 */
void Game::InitPiles() {

  // Stock + Waste piles.
  *m_stock = Pile(Pile::Kind::Stock);
  *m_waste = Pile(Pile::Kind::Waste);

  // Tableau piles.
  for (int i = 0; i < NUM_T_PILES; i++) {
    *m_tableauPiles[i] = Pile(Pile::Kind::Tableau, i+1);
  }

  // Foundation piles.
  for (int i = 0; i < NUM_F_PILES; i++) {
    *m_foundationPiles[i] = Pile(Pile::Kind::Foundation, i+1);
  }
}

/**
 * @brief Initialize static mappings by names.
 */
void Game::InitNameMaps() {

  int i;

  i = 0;
  for (Card const& card : m_state.cards) {
    m_cardIdxByName[card.name()] = i++;
  }

  i = 0;
  for (Pile const& pile : m_state.piles) {
    m_pileIdxByName[pile.name()] = i++;
  }
}


/**
 * @brief Restart game (initial state, cards are randomly shuffled anew).
 */
void Game::restart() {
  reset();
  shuffleAndDealCards();
}

/**
 * @brief Reset game to default state (empty piles etc.).
 */
void Game::reset() {

  for (Pile & pile : m_state.piles) {
    pile.removeAllCards();
  }

  for (Card & card : m_state.cards) {
    card.setUpturned(false);
  }

  m_saveName = "";
  m_history.clear();
}


/**
 * @brief Shuffle cards and assign them to piles.
 */
void Game::shuffleAndDealCards() {

  std::vector<Card*> shuffled_cards(m_cards.begin(), m_cards.end());

  std::shuffle(std::begin(shuffled_cards), std::end(shuffled_cards),
               m_RANDOM_ENGINE);

  // Assign cards to tableau piles.
  for (int i = 0; i < NUM_T_PILES; i++) {
    Pile *tableauPile = m_tableauPiles[i];

    for (int j = 0; j <= i; j++) {
      Card *card = shuffled_cards.back();
      shuffled_cards.pop_back();

      tableauPile->pushCard(card);
      // Only the last card is facing up.
      card->setUpturned(j == i);
    }
  }

  // Assign remaining cards to stock.
  while (!shuffled_cards.empty()) {
    Card *card = shuffled_cards.back();
    shuffled_cards.pop_back();

    // All stock cards are facing down (which is the default).
    m_stock->pushCard(card);
  }
}


/**
 * @brief Given the name of an upturned card, return the card.
 * @return Card if card with the given name exists and is upturned,
 *         else `nullptr`.
 */
Card *Game::getCardByName(std::string name) try {
  Card *card = &m_state.cards.at(m_cardIdxByName.at(name));
  return card->isUpturned()? card : nullptr;

} catch (std::exception) {
  return nullptr;
}

/**
 * @brief Given the name of a pile, return the pile.
 * @return Pile if pile with the given name exists, else `nullptr`.
 */
Pile *Game::getPileByName(std::string name) try {
  return &m_state.piles.at(m_pileIdxByName.at(name));

} catch (std::exception) {
  return nullptr;
}


/**
 * @brief Move a card from stock to waste, if stock is not empty.
 * @return Whether a card was moved or not (eg. false if stock was empty).
 */
bool Game::nextFromStock() {

  if (m_stock->top() == nullptr) { return false; }

  m_history.put(m_state);

  Card *card = m_stock->popCard();
  m_waste->pushCard(card);
  card->setUpturned(true);

  return true;
}

/**
 * @brief Move all cards from waste to stock, if stock is empty.
 * @return Whether the cards were moved or not (eg. true if stock was empty).
 */
bool Game::wasteToStock() {

  if (m_stock->top() != nullptr) { return false; }

  m_history.put(m_state);

  std::vector<Card*> cards = m_waste->popAllCards();
  std::reverse(cards.begin(), cards.end());

  for (Card *card : cards) {
    card->setUpturned(false);
  }

  m_stock->pushAllCards(cards);

  return true;
}


/**
 * @brief If the move valid, save state and move card. Else do nothing.
 * @param card Card to be moved.
 * @param to Pile to which to move the card.
 * @return Whether the move was made or not.
 */
bool Game::moveCard(Card *card, Pile *to) {

  if (!canMoveCard(card, to)) {
    return false;
  }

  m_history.put(m_state);
  doMoveCard(card, to);
  return true;
}

/**
 * @brief Return whether card can be moved to pile.
 * @param card Card to be moved.
 * @param pile Pile to which to move the card.
 * @return Whether the move is valid or invalid.
 */
bool Game::canMoveCard(Card *card, Pile *to) const {

  // Can only move upturned card.
  if (!card->isUpturned()) { return false; }

  Pile *from = card->pile();


  /*
   * Logic regarding the pile _from_ which the card is moved.
   */
  switch (from->kind()) {

  case Pile::Kind::Stock:
    // Cannot move from stock. Use `nextFromStock` instead.
    return false;
  break;

  case Pile::Kind::Waste:
    // When moving from waste, can only move the card on top.
    if (card != from->top()) { return false; }
  break;

  case Pile::Kind::Tableau:
    // When moving from tableau, can only move upturned card.
    // This has already been checked.
  break;

  case Pile::Kind::Foundation:
    // When moving from foundation, can only move the card on top.
    if (card != from->top()) { return false; }
  break;

  default:
    throw std::runtime_error("invalid from-pile type");
  }


  /*
   * Logic regarding the pile _to_ which the card is moved.
   */
  switch (to->kind()) {

  case Pile::Kind::Stock:
  case Pile::Kind::Waste:
    // Cannot move a card to stock or waste.
    // To move between stock and waste, `nextFromStock`
    // and `wasteToStock` functions have to be used.
    return false;
  break;

  case Pile::Kind::Tableau:
    return canMoveToTableau(card, to);
  break;

  case Pile::Kind::Foundation:
    return canMoveToFoundation(card, to);
  break;

  default:
    throw std::runtime_error("invalid to-pile type");
  }
}

/**
 * @brief Return whether card can be moved to tableau pile.
 * @param card Card to be moved.
 * @param pile Tableau pile to which to move the card.
 * @return Whether the move is valid or invalid.
 */
bool Game::canMoveToTableau(Card* card, Pile *to) const {

  // Check if the tableau pile is empty.
  if (to->top() == nullptr) {
    // Only a King can be the first card in a tableau pile.
    return card->rank() == Card::Rank::King;
  }

  // A card in a tableau pile has to be of opposite color
  // than the card underneath it, and one rank below.
  auto card_rank   = static_cast<int>( card->rank() );
  auto to_top_rank = static_cast<int>( to->top()->rank() );

  bool isOfOppositeColor = card->color() != to->top()->color();
  bool isOneRankBelow = card_rank == to_top_rank - 1;

  return isOfOppositeColor && isOneRankBelow;
}

/**
 * @brief Return whether card can be moved to foundation pile.
 * @param card Card to be moved.
 * @param pile Foundation pile to which to move the card.
 * @return Whether the move is valid or invalid.
 */
bool Game::canMoveToFoundation(Card* card, Pile *to) const {

  // Can only move the top card to a foundation pile.
  if (card != card->pile()->top()) { return false; }

  if (to->top() == nullptr) {
    // Empty pile -- the card has to be an Ace.
    return card->rank() == Card::Rank::Ace;
  }

  // Non-empty pile -- card has to be of the same suit
  // and one rank above the top of the pile.
  auto card_rank   = static_cast<int>( card->rank() );
  auto to_top_rank = static_cast<int>( to->top()->rank() );

  bool isOfSameSuit = card->suit() == to->top()->suit();
  bool isOneRankAbove = card_rank == to_top_rank + 1;

  return isOfSameSuit && isOneRankAbove;
}

/**
 * @brief Move card and all cards on top of it to a pile.
 * @param card Card to be moved.
 * @param pile Pile to which to move the cards.
 */
void Game::doMoveCard(Card *card, Pile *to) {

  Pile *from = card->pile();

  std::vector<Card*> movedCards = from->popAllCardsSince(card);

  if (Card *fromTop = from->top()) {
    fromTop->setUpturned(true);
  }

  to->pushAllCards(movedCards);
}


/**
 * @brief Provide a hint (which card can be moved).
 * @param card Set to the card which can be moved, if any.
 * @param pile Set to the pile to which a card can be moved, if any.
 * @return Whether a card can be moved or not.
 */
bool Game::hint(Card * & card, Pile * & pile) {

  for (Card & c : m_state.cards) {
    for (Pile & p : m_state.piles) {
      if (canMoveCard(&c, &p)) {
        card = &c;
        pile = &p;
        return true;
      }
    }
  }

  if (m_stock->top() != nullptr || m_waste->top() != nullptr) {
    pile = m_stock;
    return true;
  }

  return false;
}


/**
 * @brief Undo last move if possible.
 * @return Whether the move was undone or not (history is limited).
 */
bool Game::undo() {

  if (m_history.empty()) { return false; }

  m_state = m_history.pop();
  return true;
}


/**
 * @brief Return whether the game is finished (the user won).
 * @return Whether the game is finished.
 */
bool Game::isFinished() const {

  bool emptyStock = m_stock->top() == nullptr;

  bool maxOneCardInWaste = m_waste->cards().size() <= 1;

  bool allTableauCardsUpturned = true;

  for (Pile *pile : m_tableauPiles) {
    for (Card *card : pile->cards()) {
      allTableauCardsUpturned &= card->isUpturned();
    }
  }

  return emptyStock && maxOneCardInWaste && allTableauCardsUpturned;
}


/**
 * @brief Return names of all saved games.
 * @return Vector containing names of saved games.
 * @throws std::runtime_error if there was an error reading directory.
 */
std::vector<std::string> Game::getSavedGames() const {

  // Create savegame directory if it doesn't exist yet.
  mkdir(m_savePath);

  return TinyDir().getFilenames(m_savePath);
}

/**
 * @brief Check whether save of the given name already exists.
 * @param saveName Name of the saved game.
 * @throws std::runtime_error if there was an error checking the save.
 */
bool Game::saveExists(std::string saveName) const {

  auto savedGames = getSavedGames();
  auto saveNamePos = std::find(savedGames.begin(), savedGames.end(), saveName);

  return saveNamePos != savedGames.end();
}

/**
 * @brief Save current game state.
 * @param saveName Name of the saved game.
 * @throws std::runtime_error if there was an error saving the game.
 */
void Game::saveGame(std::string saveName) {

  // Create savegame directory if it doesn't exist yet.
  mkdir(m_savePath);

  saveGamePath( pathjoin(m_savePath, saveName) );
}

/**
 * @brief Save current game state.
 * @param savePath Path to the file containing the saved game.
 * @throws std::runtime_error if there was an error saving the game.
 */
void Game::saveGamePath(std::string savePath) {
  try {
    std::ofstream stream(savePath, std::ios::binary);
    cereal::JSONOutputArchive archive( stream );

    archive( m_state );
    m_saveName = savePath;
  }
  catch (std::exception) {
    throw std::runtime_error(
      "Cannot save game. Make sure the save name contains no / or \\.");
  }
}

/**
 * @brief Load game state from a saved game.
 * @param saveName Name of the saved game.
 * @throws std::runtime_error if there was an error loading the game.
 */
void Game::loadGame(std::string saveName) {
  loadGamePath( pathjoin(m_savePath, saveName) );
  m_saveName = saveName;
}

/**
 * @brief Save current game state.
 * @param savePath Path to the file into which to save the game.
 * @throws std::runtime_error if there was an error saving the game.
 */
void Game::loadGamePath(std::string savePath) {
  try {
    std::ifstream stream(savePath, std::ios::binary);
    cereal::JSONInputArchive archive( stream );

    reset();
    archive( m_state );
    m_saveName = savePath;
  }
  catch (std::exception) {
    throw std::runtime_error("Cannot load game.");
  }
}

