/**
 * @file   util.cpp
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  implementation file for utility functions & classes
 */

#include "util.h"

#include <algorithm>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <cerrno>
#include <cctype>
#include <sys/stat.h>

#ifdef _WIN32
  #inlcude <direct.h>
#endif


/**
 * @brief Create a folder; cross-platform.
 * @param path Path to folder to create.
 * @return Zero if the directory was created successfully
 *         or already existed, non-zero otherwise.
 */
int mkdir(std::string const& path) {

  const char *cpath = path.c_str();

  int ret =
#ifdef _WIN32
    _mkdir(cpath)
#else
    mkdir(cpath, 0700)
#endif
    ;

  return ret || (errno == EEXIST);
}


/**
 * @brief Join path parts; cross-platform.
 * @param path1 First path part.
 * @param path2 Second path part.
 * @return Path parts joined by the platform-specific directory separator.
 */
std::string pathjoin(std::string const& path1, std::string const& path2) {

  if (path1.back() == PATHSEP) {
    return path1 + path2;
  }
  return path1 + PATHSEP + path2;
}


/**
 * @brief Get names of all files in given folder.
 * @param path Part to the folder.
 * @return Vector of strings containing file names.
 * @throws std::runtime_error if there was an error.
 */
std::vector<std::string> TinyDir::getFilenames(std::string const& path) {

  if (tinydir_open(&dir, path.c_str()) == -1) {
    auto errstr = "cannot open path \""+path+"\"";
    throw std::runtime_error(errstr);
  }

  std::vector<std::string> filenames;

  while (dir.has_next) {

    tinydir_file file;
    if (tinydir_readfile(&dir, &file) == -1) {
      throw std::runtime_error("cannot get file in folder");
    }

    if (file.is_reg) {
      filenames.push_back(std::string(file.name));
    }

    if (tinydir_next(&dir) == -1) {
      throw std::runtime_error("cannot get next file");
    }
  }

  tinydir_close(&dir);

  return filenames;
}



/**
 * @brief Convert string to uppercase.
 * @param str Reference to string to convert.
 * @return Reference to the converted string.
 */
std::string & to_upper(std::string & str) {
  std::transform(str.begin(), str.end(), str.begin(), ::toupper);
  return str;
}

/**
 * @brief Convert string to lowercase.
 * @param str Reference to string to convert.
 * @return Reference to the converted string.
 */
std::string & to_lower(std::string & str) {
  std::transform(str.begin(), str.end(), str.begin(), ::tolower);
  return str;
}

/**
 * @brief Convert string to uppercase.
 * @param str String to convert.
 * @return The converted string.
 */
std::string upper(std::string str) {
  return to_upper(str);
}

/**
 * @brief Convert string to lowercase.
 * @param str String to convert.
 * @return The converted string.
 */
std::string lower(std::string str) {
  return to_lower(str);
}


/**
 * @brief Read a line of input from std::cin.
 * @param prompt Prompt to display before reading the input.
 * @return String containing the read line, without a newline.
 */
std::string readline(std::string const& prompt) {
  std::cout << prompt << std::flush;
  std::string line;
  std::getline(std::cin, line);
  return line;
}

/**
 * @brief Read a line of input from std::cin.
 * @param prompt Prompt to display before reading the input.
 * @return String stream containing the read line, without a newline.
 */
std::stringstream readlineStream(std::string const& prompt) {
  return std::stringstream(readline(prompt));
}


/**
 * @brief Ask for confirmation (yes / no).
 * @param prompt Prompt to display before reading the input.
 * @param def Default value (`true` -> yes, `false` -> no).
 * @return `true` if the answer was yes, `false` otherwise.
 */
bool confirm(std::string prompt, bool def) {

  static constexpr char
    YES = 'Y',
    NO  = 'N';

  const char
    y = def ? YES : std::tolower(YES),
    n = def ? std::tolower(NO) : NO;

  if (prompt != "") {
    prompt += ' ';
  }

  prompt += "[" + std::string(1,y) + '/' + std::string(1,n) + "] ";

  char rly;

  do {
    std::cout << prompt << std::flush;
    rly = std::toupper(readline()[0]);

    if (!rly) {
      rly = def ? YES : NO;
    }
  }
  while (std::cin.good() && !(rly == YES || rly == NO));

  return rly == YES;
}


/**
 * @brief Ask for confirmation (yes / no).
 * @param prompt Prompt to display before reading the input.
 * @param def Default value (`true` -> yes, `false` -> no).
 * @return `true` if the answer was yes, `false` otherwise.
 */
bool confirm(const char *prompt, bool def) {
  return confirm(std::string(prompt), def);
}

/**
 * @brief Ask for confirmation (yes / no).
 * @param def Default value (`true` -> yes, `false` -> no).
 * @return `true` if the answer was yes, `false` otherwise.
 */
bool confirm(bool def) {
  return confirm("", def);
}


