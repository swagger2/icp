/**
 * @file   pile.cpp
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  implementation file for Pile class
 */

#include "pile.h"

#include <algorithm>
#include <ostream>
#include <vector>
#include <stdexcept>


const std::map<Pile::Kind, std::string> Pile::m_kindNames = {
  {Kind::Stock,      "S"},
  {Kind::Waste,      "W"},
  {Kind::Tableau,    "T"},
  {Kind::Foundation, "F"},
};


/**
 * @brief Default constructor necessary for `std::array`.
 */
Pile::Pile() {}

/**
 * @brief Initialize the pile (for stock & waste).
 * @param kind Kind of the pile.
 */
Pile::Pile(Kind kind) :
  m_kind(kind)
{}

/**
 * @brief Initialize the pile (for tableau & foundation).
 * @param kind Kind of the pile.
 * @param num Number of the pile.
 */
Pile::Pile(Kind kind, int num) :
  m_kind(kind),
  m_num(num)
{}

/**
 * @brief Assigns all members.
 */
Pile & Pile::operator=(Pile const& other) {
  m_kind  = other.m_kind;
  m_num   = other.m_num;
  m_cards = other.m_cards;
  return *this;
}


/**
 * @return Pointer to the card on top of the pile;
 *         `nullptr` if the pile is empty.
 */
Card *Pile::top() const {
  return m_cards.empty() ? nullptr : m_cards.back();
}

/**
 * @brief Put a card on top of the the pile,
 *        setting its `pile` pointer accordingly.
 * @param card Pointer to the card to be added to the pile.
 */
void Pile::pushCard(Card *card) {
  card->setPile(this);
  m_cards.push_back(card);
}

/**
 * @brief Remove and return the card on top of the pile,
 *        setting its `pile` pointer to `nullptr`.
 * @return Pointer to the removed card.
 */
Card *Pile::popCard() {
  if (m_cards.empty()) {
    return nullptr;
  }
  Card *card = m_cards.back();
  m_cards.pop_back();

  card->setPile(nullptr);
  return card;
}

/**
 * @brief Remove and return all cards in the pile,
 *        setting their `pile` pointer to `nullptr`.
 * @return Vector of pointers to the removed cards.
 */
std::vector<Card*> Pile::popAllCards() {

  std::vector<Card*> removedCards(m_cards);
  removeAllCards();

  return removedCards;
}

/**
 * @brief Remove and return the `card` and all cards on top of
 *        the `card`, setting their `pile` pointer to `nullptr`.
 * @return Vector of pointers to the removed cards.
 */
std::vector<Card*> Pile::popAllCardsSince(Card *card) {
  auto cardPos = std::find(m_cards.begin(), m_cards.end(), card);

  std::vector<Card*> removedCards(cardPos, m_cards.end());
  m_cards.erase(cardPos, m_cards.end());

  for (Card *c : removedCards) {
    c->setPile(nullptr);
  }

  return removedCards;
}

/**
 * @brief Put multiple cards on top of the pile,
 *        setting their `pile` pointer accordingly.
 * @param cards Vector of pointers to the cards to be added to the pile.
 */
void Pile::pushAllCards(std::vector<Card*> cards) {
  for (Card *c : cards) {
    c->setPile(this);
  }
  m_cards.insert(m_cards.end(), cards.begin(), cards.end());
}

/**
 * @brief Remove all cards in the pile,
 *        setting their `pile` pointer to `nullptr`.
 */
void Pile::removeAllCards() {
  for (Card *c : m_cards) {
    c->setPile(nullptr);
  }
  m_cards.clear();
}


/**
 * @brief Returns string representation of the pile: the card
 *        on top of it if there is any, otherwise an empty string.
 * @return String representation of the pile.
 */
std::string Pile::to_string() const {
  Card *topCard = top();
  return (topCard != nullptr)? topCard->to_string() : "";
}

/**
 * @brief Returns name of the pile: its kind and also its number
 *        if it's a tableau or foundation pile.
 * @return Name of the pile.
 */
std::string Pile::name() const {

  std::string name = getKindName(m_kind);

  if (m_kind == Kind::Tableau || m_kind == Kind::Foundation) {
    name += std::to_string(m_num);
  }

  return name;
}

/**
 * @brief Get the name of a kind.
 * @param kind Kind for which to return the name.
 * @return Name of the kind.
 */
std::string Pile::getKindName(Kind kind) try {
  return m_kindNames.at(kind);
} catch (std::exception) {
  return std::to_string(static_cast<int>(kind));
}

