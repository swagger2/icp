/**
 * @file   card.cpp
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  implementation file for Card class
 */

#include "card.h"

#include <ostream>
#include <map>
#include <stdexcept>


class Pile;


const std::map<Card::Rank, std::string> Card::m_rankNames = {
  {Rank::Ace,   "A"},
  {Rank::Two,   "2"},
  {Rank::Three, "3"},
  {Rank::Four,  "4"},
  {Rank::Five,  "5"},
  {Rank::Six,   "6"},
  {Rank::Seven, "7"},
  {Rank::Eight, "8"},
  {Rank::Nine,  "9"},
  {Rank::Ten,  "10"},
  {Rank::Jack,  "J"},
  {Rank::Queen, "Q"},
  {Rank::King,  "K"},
};

const std::map<Card::Suit, std::string> Card::m_suitNames = {
  {Suit::Club,    "C"},
  {Suit::Diamond, "D"},
  {Suit::Heart,   "H"},
  {Suit::Spade,   "S"},
};


/**
 * @brief Default constructor necessary for `std::array`.
 */
Card::Card() {}

/**
 * @brief Initialize the card. Sets `color` according to `suit`.
 * @param rank The card rank.
 * @param suit The card suit.
 */
Card::Card(Rank rank, Suit suit) :
  m_rank(rank),
  m_suit(suit),
  m_color( (suit == Suit::Diamond || suit == Suit::Heart) ?
            Color::Red : Color::Black )
{}

/**
 * @brief Assigns all members.
 */
Card & Card::operator=(Card const& other) {
  m_rank  = other.m_rank;
  m_suit  = other.m_suit;
  m_color = other.m_color;
  m_isUpturned = other.m_isUpturned;
  m_pile  = other.m_pile;
  return *this;
}


/**
 * @brief Returns string representation of the card: its rank
 *        and suit if the card is facing up, otherwise obscured.
 * @return String representation of the card.
 */
std::string Card::to_string() const {
  return m_isUpturned? name() : "?";
}

/**
 * @brief Returns name of the cards: its rank and suit.
 * @return Name of the card.
 */
std::string Card::name() const {
  return getRankName(m_rank) + getSuitName(m_suit);
}


/**
 * @brief Get the name of a rank.
 * @param rank Rank for which to return the name.
 * @return Name of the rank.
 */
std::string Card::getRankName(Rank rank) try {
  return m_rankNames.at(rank);
} catch (std::exception) {
  return std::to_string(static_cast<int>(rank));
}

/**
 * @brief Get the name of a suit.
 * @param suit Suit for which to return the name.
 * @return Name of the suit.
 */
std::string Card::getSuitName(Suit suit) try {
  return m_suitNames.at(suit);
} catch (std::exception) {
  return std::to_string(static_cast<int>(suit));
}

