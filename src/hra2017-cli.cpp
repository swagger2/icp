/**
 * @file   hra2017-cli.cpp
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  CLI version of the ICP-2017 project: Klondike card game.
 */

#include "cliview.h"

/**
 * @brief Start the CliView, which implements CLI interface of the game.
 */
int main() {
  CliView().start();
}
