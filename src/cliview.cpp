/**
 * @file   cliview.cpp
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  implementation file for CLI view
 */

#include "cliview.h"

#include <stdexcept>
#include <string>
#include <sstream>
#include <cctype>
#include <cmath>


const CliView::CmdMap CliView::commandByName = {
  {"?", &CliView::help},

  {"T", &CliView::newGame},
  {"R", &CliView::restartGame},
  {"W", &CliView::closeGame},

  {"F", &CliView::prevGame},
  {"G", &CliView::nextGame},

  {"S", &CliView::saveGame},
  {"L", &CliView::loadGame},

  {"N", &CliView::dealFromStock},
  {"M", &CliView::moveCard},

  {"H", &CliView::hint},

  {"U", &CliView::undo},

  {"X", &CliView::quit},
};


/**
 * @brief Initialize the CLI interface. Max. 4 games (could be parametrized).
 */
CliView::CliView() :
  m_games(4)
{}


/**
 * @brief Create a new instance of game and star the REPL loop.
 */
void CliView::start() {
  m_games.clear();
  m_games.makeNew();
  m_currGameIdx = 0;

  m_exit = false;
  loop();
}

/**
 * @brief The REPL loop. Reads user commands and executes them.
 */
void CliView::loop() {

  show();

  while (std::cin.good() && !m_exit) {

    std::cout << std::flush;

    std::stringstream cmdStream = readlineStream("> ");
    std::string cmdStr;
    cmdStream >> cmdStr;

    if (cmdStr == "") {
      if (std::cin.good()) {
        show();
      }
      continue;
    }

    std::string cmdName = upper(cmdStr).substr(0,1);

    try {
      CmdFunc cmd = commandByName.at(cmdName);

      if ((this->*cmd)(cmdStr, cmdStream)) {
        show();
      }
    }
    catch (std::out_of_range) {
      invalidCommand(cmdStr, cmdStream);
    }
  }
}


/**
 * @brief Show game state (cards, piles, controls, ...).
 */
void CliView::show() {

  printHelp();
  std::cout << "\n\n";

  printStockWasteFoundation();
  std::cout << '\n';

  printTableau();
  std::cout << '\n';
}

/**
 * @brief Show controls (usage help).
 */
void CliView::printHelp() {
  std::cout
  << "Game: [T] new      [W] close  [F] previous  [G] next \n"
  << "      [R] restart  [S] save   [L] load      [X] exit \n"
  << "Play: [N] deal a card from stock  [M <C> <P>] move card to pile \n"
  << "      [U] undo     [H] hint \n";
}

/**
 * @brief Show stock, waste and foundation piles.
 */
void CliView::printStockWasteFoundation() {

  std::cout << '\t';
  printName(game().stock());
  printName(game().waste());
  printJust(' ');
  for (Pile *p: game().foundationPiles()) {
    printName(p);
  }
  std::cout << '\n';

  std::cout << '\t';
  printStr(game().stock());
  printStr(game().waste());
  printJust(' ');
  for (Pile *p: game().foundationPiles()) {
    printStr(p);
  }
  std::cout << '\n';
}

/**
 * @brief Show tableau piles.
 */
void CliView::printTableau() {

  std::cout << '\t';

  for (Pile *p : game().tableauPiles()) {
    printName(p);
  }
  std::cout << '\n';

  std::array<std::vector<Card*>, Game::NUM_T_PILES> tableauCards;

  std::transform(
    game().tableauPiles().begin(),
    game().tableauPiles().end(),
    tableauCards.begin(),
    [](Pile *pile) { return pile->cards(); }
  );

  for (size_t i = 0; true; i++) {

    std::cout << '\t';
    bool allDone = true;

    for (auto const& cards : tableauCards) {
      if (i < cards.size()) {
        printStr(cards[i]);
        allDone = false;
      }
      else {
        printJust(' ');
      }
    }

    if (allDone) { break; }
    std::cout << '\n';
  }
}


/**
 * @brief Print an "invalid command" message.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 */
void CliView::invalidCommand(std::string const& cmdStr, std::stringstream &) {

  std::cout << "Invalid command: \"" + cmdStr + "\". "
            << "Use '?' for help.\n";
}


/**
 * @brief End the current instance of game.
 * @return Whether there is another game instance running or not.
 */
bool CliView::closeCurrentGame() {

  if ((m_currGameIdx = m_games.remove(m_currGameIdx)) < 0) {
    exitApp();
    return false;
  }

  return true;
}

/**
 * @brief Exit the application (close all games).
 */
void CliView::exitApp() {
  std::cout << "Bye.\n";
  m_exit = true;
}


/**
 * @brief Print usage.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::help(std::string const&, std::stringstream &) {
  printHelp();
  return false;
}


/**
 * @brief Create a new instance of game, if there is room left.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::newGame(std::string const&, std::stringstream &) {

  if (!m_games.makeNew()) {
    std::cout << "Cannot play more than " << m_games.cap() << " games.\n";
    return false;
  }

  m_currGameIdx = m_games.size() - 1;
  return true;
}

/**
 * @brief Restart the current instance of game.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::restartGame(std::string const&, std::stringstream & cmdStream) {

  char rly = 0;
  cmdStream >> rly;

  if (std::toupper(rly) == 'Y' || confirm("Restart current game?")) {
    game().restart();
    return true;
  }

  return false;
}

/**
 * @brief Close the current instance of game.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::closeGame(std::string const&, std::stringstream & cmdStream) {

  char rly = 0;
  cmdStream >> rly;

  if (std::toupper(rly) == 'Y' || confirm("Close current game?")) {
    return closeCurrentGame();
  }

  return false;
}


/**
 * @brief Switch to the previous game instance if possible.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::prevGame(std::string const&, std::stringstream &) {

  if (m_currGameIdx <= 0) {
    std::cout << "No previous game.\n";
    return false;
  }

  m_currGameIdx--;
  return true;
}

/**
 * @brief Switch to the next game instance if possible.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::nextGame(std::string const&, std::stringstream &) {

  if (m_currGameIdx >= static_cast<ssize_t>( m_games.size() ) - 1) {
    std::cout << "No next game.\n";
    return false;
  }

  m_currGameIdx++;
  return true;
}


/**
 * @brief Save the current game instance.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::saveGame(std::string const&, std::stringstream & cmdStream) try {

  cmdStream >> std::ws;
  std::string saveName;

  if (cmdStream.tellg() != -1) {
    saveName = cmdStream.str().substr(cmdStream.tellg());
  }
  else {
    saveName = readline("Enter save name: ");
  }

  if (game().saveExists(saveName)) {
    if (!confirm("Save \""+saveName+"\" already exists. Overwrite?")) {
      return false;
    }
  }

  game().saveGame(saveName);
  return false;

} catch (std::exception const& e) {
  std::cout << "Error: " << e.what() << "\n";
  return false;
}

/**
 * @brief Load a saved game, replacing the current game instance.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::loadGame(std::string const&, std::stringstream &) try {

  auto savedGames = game().getSavedGames();

  if (savedGames.empty()) {
    std::cout << "No saved games.\n";
    return false;
  }

  int w = static_cast<int>( log10(savedGames.size()) ) + 1;

  for (size_t i = 0; i < savedGames.size(); i++) {
    std::cout << " [" << std::right << std::setw(w) << (i+1) << "] "
              << savedGames[i] << "\n";
  }

  long int idx = 0,
           numGames = savedGames.size();

  while (std::cin.good()) {
    std::string idxStr = readline(">>> ");

    if (idxStr == "") {
      std::cout << "Cancelled.\n";
      return false;
    }

    char *ep;
    idx = strtol(idxStr.c_str(), &ep, 10) - 1;

    if (!*ep && idx >= 0 && idx < numGames) {
      break;
    }

    std::cout << "Enter a number from the list.\n";
  }

  game().loadGame(savedGames[idx]);
  return true;

} catch (std::exception const& e) {
  std::cout << "Error: " << e.what() << "\n";
  return false;
}


/**
 * @brief Deal a card from stock to waste.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::dealFromStock(std::string const&, std::stringstream &) {

  if (!game().nextFromStock()) {
    game().wasteToStock();
  }

  return true;
}

/**
 * @brief Move a card to another pile.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::moveCard(std::string const&, std::stringstream & cmdStream) {

  std::string cardNameOrig, pileNameOrig;
  cmdStream >> cardNameOrig >> pileNameOrig;

  std::string
    cardName = upper(cardNameOrig),
    pileName = upper(pileNameOrig);

  Card *card = game().getCardByName(cardName);

  if (card == nullptr) {
    Pile *cardPile = game().getPileByName(cardName);

    if (cardPile == nullptr || (card = cardPile->top()) == nullptr) {
      std::cout << "Invalid card: \"" << cardNameOrig << "\"\n";
      return false;
    }
  }

  Pile *pile = game().getPileByName(pileName);

  if (pile == nullptr) {
    Card *pileCard = game().getCardByName(pileName);

    if (pileCard == nullptr
     || (pile = pileCard->pile())->top() != pileCard
    ) {
      std::cout << "Invalid pile: \"" << pileNameOrig << "\"\n";
      return false;
    }
  }

  if (!game().moveCard(card, pile)) {
    std::cout << "Invalid move. Check the rules.\n";
    return false;
  }

  if (!game().isFinished()) {
    return true;
  }

  std::cout << "Congratulations! You won!\n\n";

  if (confirm("Play again?", true)) {
    game().restart();
    return true;
  }

  return closeCurrentGame();
}


/**
 * @brief Show a possible move.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::hint(std::string const&, std::stringstream &) {

  Card *card;
  Pile *pile;

  if (!game().hint(card, pile)) {
    std::cout << "No move possible.\n";
    return false;
  }

  if (pile == game().stock()) {
    std::cout << "Deal from stock.\n";
  }
  else {
    std::cout << card->name() << " -> " << pile->name() << "\n";
  }
  return false;
}


/**
 * @brief Undo the last move in the current game.
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::undo(std::string const&, std::stringstream &) {

  if (!game().undo()) {
    std::cout << "Cannot undo!\n";
    return false;
  }

  return true;
}


/**
 * @brief Exit the application (close all games).
 * @param cmdStr Original command name as entered by the user.
 * @param cmdStream Remaining arguments.
 * @return Whether to repaint the playing board or not.
 */
bool CliView::quit(std::string const&, std::stringstream & cmdStream) {

  char rly = 0;
  cmdStream >> rly;

  if (std::toupper(rly) == 'Y' || confirm("Close all games and exit?")) {
    exitApp();
  }

  return false;
}


