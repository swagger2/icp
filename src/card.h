/**
 * @file   card.h
 * @author Matej Turcel (xturce00@sutd.fit.vutbr.cz)
 * @brief  header file for Card class
 */

#ifndef CARD_H
#define CARD_H

#include <string>
#include <map>


class Pile;


/**
 * @brief Represents a card. Contains only the information about a card,
 *        plus a pointer to the pile in which the card is.
 */
class Card {

public: // enums & classes

  /**
   * @brief Card ranks.
   */
  enum class Rank {
    Ace,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    COUNT
  };

  /**
   * @brief Card suits.
   */
  enum class Suit {
    Club,
    Diamond,
    Heart,
    Spade,
    COUNT
  };

  /**
   * @brief Card colors.
   */
  enum class Color {
    Black,
    Red,
    COUNT
  };

public: // methods

  Card();
  Card(Rank rank, Suit suit);

  Card & operator=(Card const& other);


  std::string to_string() const;
  std::string name() const;


  /**
   * @return Card rank.
   */
  Rank  rank()  const { return m_rank; }

  /**
   * @return Card suit.
   */
  Suit  suit()  const { return m_suit; }

  /**
   * @return Card color.
   */
  Color color() const { return m_color; }


  /**
   * @return `true` if the card is facing up, `false` otherwise.
   */
  bool isUpturned() const { return m_isUpturned; }

  /**
   * @param isUpturned `true` to turn the card face-up,
   *        `false` to turn the card face-down.
   */
  void setUpturned(bool isUpturned) { m_isUpturned = isUpturned; }


  /**
   * @return Pointer to the pile in which the card is.
   */
  Pile *pile() const { return m_pile; }

  /**
   * @param pile Pointer to the pile into which to put the card.
   */
  void setPile(Pile *pile) { m_pile = pile; }

public: // static methods

  /**
   * @return Mapping of rank names to ranks.
   */
  static std::map<Rank, std::string> const& rankNames() {
    return m_rankNames;
  }

  /**
   * @return Mapping of suit names to suits.
   */
  static std::map<Suit, std::string> const& suitNames() {
    return m_suitNames;
  }

  static std::string getRankName(Rank rank);
  static std::string getSuitName(Suit suit);

private: // static members

  static const std::map<Rank, std::string> m_rankNames;
  static const std::map<Suit, std::string> m_suitNames;

private: // instance members

  Rank  m_rank  = Rank::COUNT;
  Suit  m_suit  = Suit::COUNT;
  Color m_color = Color::COUNT;

  bool m_isUpturned = false;

  Pile *m_pile = nullptr;
};


#endif

