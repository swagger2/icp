#
# Makefile for the ICP project -- klondike game.
#
# Author: Matej Turcel (xturce00@sutd.fit.vutbr.cz)
#

CXX      ?= g++
CXXFLAGS := -std=c++11 -Wall -Wextra -pedantic -g -Iinc

src := src
obj := obj
doc := doc
examples := examples

BIN_GUI   := hra2017
BIN_CLI   := $(BIN_GUI)-cli
BIN_FILES := $(BIN_GUI) $(BIN_CLI)

SRC_FILES_ALL := $(wildcard $(src)/*.cpp)
SRC_FILES := $(filter-out \
							$(wildcard $(src)/$(BIN_GUI).cpp \
								$(src)/$(BIN_CLI).cpp $(src)/cliview.cpp), \
							$(SRC_FILES_ALL))

OBJ_FILES := $(addprefix $(obj)/,$(notdir $(SRC_FILES:.cpp=.o)))

GEN_SUBFOLDERS := $(obj)/* $(doc)/*
GEN_ALL        := $(BIN_FILES) $(GEN_SUBFOLDERS)

PACK_FILE := xturce00-xmores00.zip


.PHONY: all clean run


all: $(BIN_FILES)

clean:
	rm -rf $(GEN_ALL) $(PACK_FILE)

doxygen:
	-ln -sf README.txt README.md
	doxygen

pack:
	rm -rf $(GEN_SUBFOLDERS)
	zip -FS9r $(PACK_FILE) * -x *.git* $(BIN_FILES)

run: $(BIN_FILES)
	@(cd examples && exec ../hra2017-cli)


$(BIN_GUI): $(obj)/$(BIN_GUI).o $(OBJ_FILES)
	$(CXX) $(CXXFLAGS) -o $@ $^

$(BIN_CLI): $(obj)/$(BIN_CLI).o $(obj)/cliview.o $(OBJ_FILES)
	$(CXX) $(CXXFLAGS) -o $@ $^

$(obj)/%.o: $(src)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<


CXXFLAGS += -MMD
-include $(OBJ_FILES:.o=.d)

