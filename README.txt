
# Projekt do predmetu ICP: HRA 2017

Táto aplikácia implementuje kartovú hru Pasians (Solitaire) Klondike.
Bližšie informácie sú k dispozícii na Wikipédii
(https://en.wikipedia.org/wiki/Klondike_(solitaire)).


## Preklad

Preklad sa spustí zadaním príkazu `make` v koreňovom adresári.
Prekladom sa v koreňovom adresári vytvoria spustiteľné súbory
"hra2017" s grafickým rozhraním a "hra2017-cli" s textovým rozhraním.

Pre vygenerovanie dokumentácie je potrebné zadať príkaz `make doxygen`.
Jeho zadaním sa vygeneruje HTML dokumentácia v adresári "doc". Nápoveď je
možné zobraziť vo webovom prehliadači otvorením súboru "doc/html/index.html".


## Spustenie

Aplikáciu je možné spustiť priamo, napr. `./hra2017-cli`, alebo príkazom
`make run`. Príkaz `make run` najprv vykoná preklad a potom spustí aplikáciu
v zložke "examples", aby bolo možné načítať pripravené uložené hry.
Tie aplikácia načíta zo zložky "savegames". Táto zložka je automaticky
vytvorená pri spustení aplikácie.


## Použitie

### Textové rozhranie

V textovom rozhraní sú na hracej ploche zobrazené názvy kariet a kôp.

Názvy kariet sú vo formáte "<Rank><Suit>":
- <Rank> je jedna z možností "A", čísla 2 - 10, "J", "Q", "K",
- <Suit> je jedna z možností "C", "D", "H", "S".

Názvy kôp sú vo formáte "<Kind>[<num>]":
- "S" -- kopa 'stock',
- "W" -- kopa 'waste',
- "T<num>" -- kopa 'tableau' číslo <num>,
- "F<num>" -- kopa 'foundation' číslo <num>.

Aplikácia sa ovláda textovými príkazmi. U príkazov nezáleží na veľkosti
písmen (tj. príkazy sú case-insensitive). Relevantné je iba prvé písmeno
príkazu -- napríklad príkaz "M" je totožný s príkazom "mv" alebo "move".

Karty sa presúvajú medzi kopami príkazom "M <Card> <Pile>",
kde <Card> je názov presúvanej karty a <Pile> je názov kopy, na ktorú
má byť karta presunutá. V niektorých prípadoch je možné zadať namiesto
názvu kopy názov karty, alebo naopak:
- Ak je namiesto názvu karty zadaný názov neprázdnej kopy, bude použitá
  karta na vrchu tejto kopy. Napr. ak sa na vrchu kopy "T1" nachádza karta
  "JH", príkaz "M T1 F2" presunie kartu "JH" na kopu "F2".
- Ak je namiesto názvu kopy zadaný názov vrchnej karty niektorej kopy,
  bude použitá táto kopa. Napr. ak sa na vrchu kopy "T1" nachádza karta
  "JH", príkaz "M 10S JH" presunie kartu "10S" na kopu "T1".

Niektoré príkazy vyžadujú potvrdenie. Takéto príkazy je možné potvrdiť
automaticky, zadaním argumentu "Y" za príkazom. Napríklad pre okamžité
ukončenie aplikácie je možné zadať príkaz "X Y".

Všetky dostupné príkazy sú popísané v samotnej aplikácii.
Zadaním príkazu "?" sa zobrazí nápoveď k ovládaniu aplikácie.


## Autori

- Matej Turcel (xturce00@stud.fit.vutbr.cz) -- logika hry a textové rozhranie
- Martin Mores (xmores00@stud.fit.vutbr.cz) -- grafické rozhranie

